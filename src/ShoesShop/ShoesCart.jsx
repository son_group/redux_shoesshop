import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DECREASE_PRODUCT,
  INCREASE_PRODUCT,
  REMOVE_PRODUCT,
} from "../constants/shoesConstants";

class ShoesCart extends Component {
  renderCart = () => {
    return this.props.shoesCart.map((shoes, index) => {
      console.log("shoes", shoes);
      return (
        <tr key={index}>
          <td>{shoes.id}</td>
          <td>{shoes.name}</td>
          <td>
            <img src={shoes.image} width={80} alt="" />
          </td>
          <td>{shoes.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleDecrease(shoes);
              }}
              type="button"
              className="btn btn-secondary mr-2"
            >
              -
            </button>
            {shoes.quantity}
            <button
              onClick={() => {
                this.props.handleIncrease(shoes);
              }}
              type="button"
              className="btn btn-secondary ml-2"
            >
              +
            </button>
          </td>
          <td>{shoes.price * shoes.quantity}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(shoes);
              }}
              type="button"
              className="btn btn-danger ml-2"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container mt-5">
        <table className="table">
          <thead>
            <tr>
              <th>Mã sp</th>
              <th>Tên sp</th>
              <th>Hình ảnh</th>
              <th>Giá bán</th>
              <th>Số lượng</th>
              <th>Thành tiền</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoesCart: state.shoesReducer.shoesCart,
  };
};

let mapDispatchToProp = (dispatch) => {
  return {
    handleIncrease: (shoes) => {
      let action = {
        type: INCREASE_PRODUCT,
        payload: shoes,
      };

      dispatch(action);
    },
    handleDecrease: (shoes) => {
      let action = {
        type: DECREASE_PRODUCT,
        payload: shoes,
      };

      dispatch(action);
    },
    handleRemove: (shoes) => {
      let action = {
        type: REMOVE_PRODUCT,
        payload: shoes,
      };

      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProp)(ShoesCart);
