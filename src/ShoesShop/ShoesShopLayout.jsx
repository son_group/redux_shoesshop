import React, { Component } from "react";
import ShoesCart from "./ShoesCart";
import ListShoes from "./ListShoes";
import ShoesDetail from "./ShoesDetail";

export default class ShoesShopLayout extends Component {
  render() {
    return (
      <div className="container">
        <h3 className="text-center">SHOES SHOP</h3>
        <ShoesCart />
        <ListShoes />
        <ShoesDetail />
      </div>
    );
  }
}
