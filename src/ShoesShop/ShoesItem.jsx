import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, VIEW_DETAIL } from "../constants/shoesConstants";

class ShoesItem extends Component {
  render() {
    let shoes = this.props.shoesItem;
    return (
      <div className="card m-3" style={{ width: "20rem" }}>
        <img className="card-img-top" src={shoes.image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{shoes.name}</h5>
          <p className="card-text">
            {shoes.description.length > 100
              ? shoes.description.substring(0, 100)
              : shoes.description}
          </p>
          <div
            className="btn-group mr-2"
            role="group"
            aria-label="Second group"
          >
            <button
              onClick={() => {
                this.props.handleViewDetail(shoes);
              }}
              type="button"
              className="btn btn-secondary mr-5 rounded"
            >
              View detail
            </button>

            <button
              onClick={() => {
                this.props.handleAddToCart(shoes);
              }}
              type="button"
              className="btn btn-primary rounded"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProp = (dispatch) => {
  return {
    handleAddToCart: (shoes) => {
      let shoesItem = {
        id: shoes.id,
        name: shoes.name,
        price: shoes.price,
        quantity: 1,
        image: shoes.image,
      };
      let action = {
        type: ADD_TO_CART,
        payload: shoesItem,
      };

      dispatch(action);
    },
    handleViewDetail: (shoes) => {
      let shoesItem = {
        id: shoes.id,
        name: shoes.name,
        price: shoes.price,
        image: shoes.image,
        description: shoes.description,
      };
      let action = {
        type: VIEW_DETAIL,
        payload: shoesItem,
      };

      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProp)(ShoesItem);
