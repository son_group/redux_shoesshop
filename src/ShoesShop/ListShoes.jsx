import React, { Component } from "react";
import { connect } from "react-redux";
import ShoesItem from "./ShoesItem";

class ListShoes extends Component {
  render() {
    return (
      <div className=" container">
        <div className="row">
          {this.props.shoesArr.map((shoes, index) => {
            return <ShoesItem shoesItem={shoes} key={index} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoesArr: state.shoesReducer.shoesArr,
  };
};

export default connect(mapStateToProps, null)(ListShoes);
