import React, { Component } from "react";
import { connect } from "react-redux";

class ShoesDetail extends Component {
  render() {
    let { name, price, description, image } = this.props.detailShoes;
    return (
      <div className="container">
        <div className="row">
          <div className="col-4 ">
            <img src={image} width={350} />
          </div>
          <div className="col-8 text-left text-secondary ">
            <p className="mt-5">
              <span className="font-weight-bold"> Product name:</span> {name}
            </p>
            <p>
              <span className="font-weight-bold">Price:</span> ${price}
            </p>
            <p>
              <span className="font-weight-bold">Description:</span>
              {description}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detailShoes: state.shoesReducer.detailShoes,
  };
};

export default connect(mapStateToProps)(ShoesDetail);
