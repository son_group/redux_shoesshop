import {
  VIEW_DETAIL,
  ADD_TO_CART,
  INCREASE_PRODUCT,
  DECREASE_PRODUCT,
  REMOVE_PRODUCT,
} from "../../constants/shoesConstants";
import { dataShoes } from "../../data";

let initialState = {
  shoesArr: dataShoes,
  detailShoes: dataShoes[0],
  shoesCart: [],
};

export let shoesReducer = (state = initialState, action) => {
  console.log("reducer ", state.shoesCart);
  switch (action.type) {
    case VIEW_DETAIL: {
      state.detailShoes = action.payload;
      return { ...state };
    }
    case ADD_TO_CART: {
      let shoesCartUpdate = [...state.shoesCart];
      let index = shoesCartUpdate.findIndex((shoes) => {
        return shoes.id === action.payload.id;
      });
      if (index === -1) {
        shoesCartUpdate.push(action.payload);
      } else {
        shoesCartUpdate[index].quantity += 1;
      }

      state.shoesCart = shoesCartUpdate;
      console.log("state", state);
      return { ...state };
    }
    case INCREASE_PRODUCT: {
      let shoesCartUpdate = [...state.shoesCart];
      let index = shoesCartUpdate.findIndex((shoes) => {
        return shoes.id === action.payload.id;
      });
      if (index !== -1) {
        shoesCartUpdate[index].quantity += 1;
      }
      state.shoesCart = shoesCartUpdate;

      return { ...state };
    }
    case DECREASE_PRODUCT: {
      let shoesCartUpdate = [...state.shoesCart];
      let index = shoesCartUpdate.findIndex((shoes) => {
        return shoes.id === action.payload.id;
      });
      console.log("index", index);
      if (index !== -1 && shoesCartUpdate[index].quantity > 1) {
        shoesCartUpdate[index].quantity -= 1;
      }
      state.shoesCart = shoesCartUpdate;

      return { ...state };
    }

    case REMOVE_PRODUCT: {
      let shoesCartUpdate = [...state.shoesCart];
      let index = shoesCartUpdate.findIndex((shoes) => {
        return shoes.id === action.payload.id;
      });
      console.log("index", index);
      if (index !== -1) {
        shoesCartUpdate.splice(index, 1);
      }
      state.shoesCart = shoesCartUpdate;

      return { ...state };
    }
    default:
      return state;
  }
};
