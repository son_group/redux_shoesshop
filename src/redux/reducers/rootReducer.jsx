import { combineReducers } from "redux";

import { shoesReducer } from "./shoesReducers";

export const rootReducer = combineReducers({
  shoesReducer: shoesReducer,
});

// export const rootReducer = combineReducers({
//   GioHangReducer: GioHangReducer,
// });
