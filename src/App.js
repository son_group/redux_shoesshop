import "./App.css";

import ShoesShopLayout from "./ShoesShop/ShoesShopLayout";

function App() {
  return (
    <div className="App">
      <ShoesShopLayout />
    </div>
  );
}

export default App;
